# Personnal dotfiles

### Setup script
The script will create all the necessary symbolic links and push the gnome shortcuts.

```
chmod +x setup.sh
./setup.sh
```

### Example of symlink creation

```
mv ~/.config/nvim/init.vim  ~/<path-to-folder>/dotfiles/nvim/init.vim 
ln -sf ~/<path-to-folder>/nvim/init.vim  ~/.config/nvim/init.vim 
```

### Gnome keyboard shortcuts

```
# Save: 
dconf load /org/gnome/desktop/wm/keybindings/ > keybindings.dconf
# Load: 
dconf load /org/gnome/desktop/wm/keybindings/ < keybindings.dconf
```
