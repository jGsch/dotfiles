filetype off                  " required

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vundle: configuration
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin()

" Plugins
Plugin 'gmarik/Vundle.vim'

Plugin 'itchyny/lightline.vim'                    "status bar

Plugin 'neoclide/coc.nvim', {'branch': 'release'}

Plugin 'scrooloose/nerdtree'                      "file system explorer
Plugin 'preservim/nerdcommenter'                  "comments
Plugin 'morhetz/gruvbox'                          "color scheme
Plugin 'christoomey/vim-tmux-navigator'           "nav
Plugin 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }

call vundle#end()
filetype plugin indent on


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user:interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Show line number
set number

" Show matching brackets when text indicator is over them
set showmatch 

" Do smart autoindenting when starting a new line 
set smartindent

" Set number of spaces per auto indentation
set shiftwidth=2

" When using <Tab>, put spaces instead of a <tab> character
set expandtab 

" Number of spaces that a <Tab> in the file counts for
set tabstop=2  

" At <Tab> at beginning line inserts spaces set in shiftwidth
set smarttab    

" To avoid issues with file sharing with docker
set backupcopy=yes

" Disable swap files 
set noswapfile

set signcolumn=number

if has('persistent_undo')      "check if your vim version supports it
  set undofile                 "turn on the feature  
  set undodir=$HOME/.config/nvim/undo  "directory where the undo files will be stored
  endif     


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Status bar: config 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Activate status bar
set laststatus=2

" Remove delay when switching mode
set ttimeoutlen=50

" Remove unnecessary anymore displayed mode information 
set noshowmode

" Change status bar colorscheme
let g:lightline = {'colorscheme': 'gruvbox',}


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => File system explorer: config 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Open/Close NERDTree 
map <C-n> :NERDTreeToggle<CR>

" Open in a new tab
let NERDTreeMapOpenInTab="<leader>tn"


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable syntax highlighting
syntax on

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Set background color 
set background=dark
"set background=light

" Change color scheme
colorscheme gruvbox

" Enable true color 
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => COC.nvim
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Update sign column every quarter second
set updatetime=300

" no error when comment in json
autocmd FileType json syntax match Comment +\/\/.\+$+

" yank list
nnoremap <silent> <C-p> :<C-u>CocList --normal yank<cr>

" navigate chunks of current buffer
nmap .g <Plug>(coc-git-prevchunk)
nmap -g <Plug>(coc-git-nextchunk)

" info / undo / commit changes 
nmap gs :CocCommand git.chunkInfo<CR>
nmap gu :CocCommand git.chunkUndo<CR>
nmap gc :CocCommand git.chunkStage<CR>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Keyboard shortchuts 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Change leader key and add more time to enter keys 
let mapleader=","
set timeout timeoutlen=500

" Map <Space> to / (search) and Ctrl-<Space> to ? (backwards search)
map <space> /
map <C-space> ?

" Switch CWD to the directory of the open buffer:
map <leader>cd :cd %:p:h<cr>:pwd<cr>

" Disable highlights when you press <leader><cr>:
map <silent> <leader><cr> :noh<cr>

" Insert pdb trace
nnoremap <leader>p oimport ipdb; ipdb.set_trace()<Esc>

" Escape at jj 
imap jj <Esc>

" To not use arrows anymore
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" Activate spell checking:
map <leader>ss :setlocal spell!<cr>

" Better key to move one paragraph
map $ }
map à {
