#!/bin/bash

## Create symlink

mkdir -p ~/.config/terminator
ln -sfv "${PWD}/terminator/config" ~/.config/terminator/

mkdir -p ~/.config/nvim
ln -sfv "${PWD}/nvim/init.vim" ~/.config/nvim/


## Load gnome keybindings
dconf load /org/gnome/desktop/wm/keybindings/ < "${PWD}/gnome_keyboard_shortcuts/keybindings.dconf"
